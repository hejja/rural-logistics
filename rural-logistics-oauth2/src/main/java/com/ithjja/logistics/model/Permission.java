package com.ithjja.logistics.model;

import lombok.Data;

/**
 * @author Administrator
 * @version 1.0
 **/
@Data
public class Permission {

    private String id;

    private String permName;

    private String permTag;

    private String url;
}
