package com.ithjj.logistics.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.One;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.ResultMap;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import com.ithjj.logistics.model.Town;

/**
 * @author hejja
 */
@Mapper
public interface TownMapper {

    /**
     * 添加城镇
     * @param town 城镇信息
     */
    @Insert("insert into tbl_town values(#{id}, #{cid}, #{name}, #{deleted}, #{createTime})")
    void addTown(Town town);

    /**
     * 修改城镇信息
     * @param town 城镇信息
     */
    @Update("update tbl_town set cid = #{cid}, name = #{name} where id = #{id}")
    void updateTown(Town town);

    /**
     * 查询所有城镇信息
     * @return 城镇集合
     */
    @Select("select * from tbl_town where deleted = false")
    @ResultMap("cityResultMapping")
    List<Town> findAll();

    /**
     * 根据城市id查询城镇信息
     * @param cid 城市id
     * @return 城镇信息
     */
    @Select("select * from tbl_town where cid = #{cid} and deleted = false")
    @Results(id = "defaultResultMapping", value = {
            @Result(id = true, column = "id", property = "id"),
            @Result(column = "cid", property = "cid"),
            @Result(column = "name", property = "name"),
            @Result(column = "deleted", property = "deleted"),
            @Result(column = "create_time", property = "createTime")
    })
    List<Town> findByCid(@Param("cid") Long cid);

    /**
     * 根据城镇id查询城镇信息
     * @param id 城镇id
     * @return 城镇信息
     */
    @Select("select * from tbl_town where id = #{id}")
    @Results(id = "cityResultMapping", value = {
        @Result(id = true, column = "id", property = "id"),
        @Result(column = "cid", property = "cid"),
        @Result(column = "name", property = "name"),
        @Result(column = "deleted", property = "deleted"),
        @Result(column = "create_time", property = "createTime"),
            @Result(column = "cid", property = "city",
                one = @One(select = "com.ithjj.logistics.mapper.CityMapper.findByIdDefault"))
    })
    Town findById(@Param("id") Long id);

}
