package com.ithjja.logistics.common.consts;

/**
 * 文件类型常量类
 * @author hejja
 */
public class DocumentConsts {

    /** 农作物类型 */
    public static final Integer CROPS_TYPE = 1;
}
