package com.ithjja.logistics.common.result;

import lombok.Data;

import java.io.Serializable;

/**
 * 通用返回结果
 * @author hejja
 */
@Data
public class Result<T> implements Serializable {

    private String code;

    private String msg;

    private T data;

    private long total;

    public Result() {

    }

    public static <T> Result<T> success() {
        return success(null);
    }

    public static <T> Result<T> success(T data) {
        return result("200", "success", data);
    }

    public static <T> Result<T> failed(String code, String message) {
        return result(code, message, null);
    }

    public static <T> Result<T> result(String code, String msg, T data) {
        Result<T> result = new Result<>();
        result.setCode(code);
        result.setData(data);
        result.setMsg(msg);
        return result;
    }
}
