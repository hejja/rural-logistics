package com.ithjja.logistics.web.advice;

import cn.hutool.core.lang.Dict;

import java.util.Map;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import com.ithjja.logistics.common.ex.ExpectedException;

/**
 * @author hejja
 */
@ControllerAdvice(annotations = RestController.class)
public class WebExceptionHandler extends ResponseEntityExceptionHandler {

    @ExceptionHandler(ExpectedException.class)
    public ResponseEntity<Map<String, Object>> handleExpectedException(ExpectedException ee) {
        return ResponseEntity.badRequest().body(Dict.create().set("message", ee.getMessage()));
    }
}
